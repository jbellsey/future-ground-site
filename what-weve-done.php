<?php 
	require_once("lib/template.php");
	$webRoot = $tmpl->webRoot();
	
	$tmpl->title = "Portfolio of Web Projects, Wordpress development, Technology Consulting";
	$tmpl->metaDesc = "";
	$tmpl->activeNav = "whatWeveDone";
	$tmpl->bgPicture = "pic-shuttle";
	$tmpl->installCSS( $webRoot . "/css/what-weve-done.css");
	$tmpl->printTop();
?>

<div id="main">
	<div id="basicPage">
		
		<h2>Cap, meet feathers. Feathers, cap.</h2>
		
		<p class="big">These projects run the gamut. Everything from lightweight
			adjustment of a prebuilt Wordpress theme, to customized design
			and implementation, to product launches and integrations of multiple
			external services. Tour around at your own pace...we’re not going anywhere.</p>
		
		<?php if(0) { ?>
					<div class="entry">
						<a href="<?php echo $webRoot ?>/portfolio/cito">
							<img src="<?php echo $webRoot ?>/pfolio/icons/cito.png">
								<h4>Calling In The One</h4>
								<p>Web site, product launch, and teleseminar. Built for
									one of the best relationship coaches in the business.</p>
						</a>
					</div>
		<?php } ?>

        <div class="entry">
            <a href="<?php echo $webRoot ?>/portfolio/dogdecoder">
                <img src="<?php echo $webRoot ?>/pfolio/icons/dog.png">
                <h4>Dog Decoder</h4>
                <p>A dog-training iPhone app. But it’s not the dog that gets
                    trained; it’s you.</p>
            </a>
        </div>

        <div class="entry">
            <a href="<?php echo $webRoot ?>/portfolio/vital-medicine">
                <img src="<?php echo $webRoot ?>/pfolio/icons/vital-med.png">
                <h4>Vital Medicine</h4>
                <p>A complete web and business reboot for an innovative naturopathic healer.</p>
            </a>
        </div>

        <div class="entry">
			<a href="<?php echo $webRoot ?>/portfolio/enlightennext">
				<img src="<?php echo $webRoot ?>/pfolio/icons/enx.png">
					<h4>EnlightenNext</h4>
					<p>Thousands of pages, millions of page views, two Webby
						awards. Web site for a cutting-edge magazine bridging
						spirituality, culture, and science.</p>
			</a>
		</div>

		<div class="entry">
			<a href="<?php echo $webRoot ?>/portfolio/reinvention">
				<img src="<?php echo $webRoot ?>/pfolio/icons/reinvention.png">
					<h4>Reinvention Summit</h4>
					<p>Strategy and technology consulting for the biggest online storytelling
						event ever.</p>
			</a>
		</div>
		
		<div class="entry">
			<a href="<?php echo $webRoot ?>/portfolio/houston">
				<img src="<?php echo $webRoot ?>/pfolio/icons/houston.png">
					<h4>Your Life’s Purpose</h4>
					<p>Jean Houston’s online course. Self-paced learning, downloadable
						audios, and a vibrant community platform.</p>
			</a>
		</div>

		<?php if(0) { ?>
				<div class="entry">
					<a href="<?php echo $webRoot ?>/portfolio/embodiment">
						<img src="<?php echo $webRoot ?>/pfolio/icons/embodiment.png">
							<h4>Enlightened Embodiment</h4>
							<p>Web design and technology for a sexologist, transpersonal
								psychologist, and workshop leader. (Yes, it’s safe for work.)</p>
					</a>
				</div>
		<?php } ?>

		<div class="entry">
			<a href="<?php echo $webRoot ?>/portfolio/cohen">
				<img src="<?php echo $webRoot ?>/pfolio/icons/cohen.png">
					<h4>Andrew Cohen</h4>
					<p>Spiritual teacher, controversial visionary, retreat leader
						and author. Complex web presence? You bet.</p>
			</a>
		</div>
		
		<div class="entry">
			<a href="<?php echo $webRoot ?>/portfolio/mccart">
				<img src="<?php echo $webRoot ?>/pfolio/icons/mccart.png">
					<h4>Bill McCart Consulting</h4>
					<p>Hand-crafted Wordpress site for a new consulting business.</p>
			</a>
		</div>
		
		<div style="clear:both;"></div>
	</div>
</div>


<?php 
	$tmpl->printBottom();