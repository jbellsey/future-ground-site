<?php 
	require_once("lib/template.php");
	
	$tmpl->title = "Future Ground | Web site development, technology consulting, custom app development in Marin, CA";
	$tmpl->metaDesc = "";
	$tmpl->layout = "home";
	$tmpl->activeNav = "";
	$tmpl->installCSS( $tmpl->webRoot() . "/css/home.css");

	$tmpl->printTop();
?>

<div id="main">

	<div id="copy" class="big">
		<h1>Future Ground</h1>

		<h2>Business apps and technology <br>for the space age.</h2>

		<p>From a custom business suite to small web sites to six-figure product launches, from
		back-end tech support to high-level strategic advice, we&rsquo;ve got you covered.</p>
		
		<p>Surf around to see what we do, the kudos we&rsquo;ve received
		(including two Webby awards), and how we can help you launch
		your next project to the moon.</p>
		
		<a class="orange" href="<?php echo $tmpl->webRoot() ?>/contact">Click here to discuss your project</a>

		<div id="icons">
			Shopping carts
			Social media
			Email strategy
			Web sites
			Twitter integration
			Web video
			Blogs
			Web site analytics
		</div>
	</div>

	<div id="planets">
		<div id="p1"> </div>
		<div id="p2"> </div>
		<div id="p3"> </div>	
	</div>

</div>

<?php 
	$tmpl->printBottom();
?>