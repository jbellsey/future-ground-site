<?php 
	require_once("lib/template.php");
	$webRoot = $tmpl->webRoot();
	
	$tmpl->title = "Testimonials";
	$tmpl->metaDesc = "";
	$tmpl->activeNav = "testim";
	$tmpl->bgPicture = "pic-naut";
	$tmpl->installCSS( $webRoot . "/css/testim.css");
	$tmpl->printTop();
?>

<div id="main">
	<div id="basicPage" class="big">
		
		<h2>Testimonials</h2>
		
		<p>You won’t find this page filled with quotes about how good our technology is; the work speaks for itself. Instead, these are comments about what it’s like to work with us, because while we obviously do technology, what we really do is partnership. But first, a big testimonial:</p>
		
		<blockquote class="webby">
			<h3>Webby Award, 2006 and 2007 (People’s Voice)<br>
				Best Religion &amp; Spirituality Web Site</h3>
			<p>Any award this big is clearly a team effort. But it’s also an incredible honor. EnlightenNext.org was chosen for its content, organization, design, and overall awesomeness in a space not normally known for great web sites.</p>
			<p>Then it was chosen again.</p>
		</blockquote>
		
		<blockquote><p>I’m buzzing from our conversation, and feel like a lid has been lifted...like the visions and sense of where I see myself going and what I see myself offering isn’t some far off dream, but actually now feels like it has some ground, traction and a path to manifest.</p>
			<cite>Deb Z., Entrepreneur</cite>
		</blockquote>
		
		<blockquote><p>I just want to say that in every meeting I have attended at which you have also been present, you have been a voice of (often singularly) complete sanity in an otherwise chaotic environment. </p>
			<cite>Lauren T., Manager, Customer Service</cite>
		</blockquote>
		
		<blockquote><p>You’re one of the brightest, most talented people I’ve ever met, even though I don’t really understand what it is that you do!</p> 
			<cite>Claire Z., Business Owner &amp; Spiritual Teacher</cite>
		</blockquote>
		
		<?php /* 
		<p>You are incredibly brilliant!  I've always known it, but to see it manifesting is just
        dazzling.  I am so thankful for your clarity and rigor.</p> <!--Deborah B-->
		*/ ?>

	</div>
</div>

<?php 
	$tmpl->printBottom();