<?php 
	require_once("lib/template.php");
	$webRoot = $tmpl->webRoot();
	
	$tmpl->title = "Contact Us";
	$tmpl->metaDesc = "";
	$tmpl->activeNav = "contact";
	$tmpl->bgPicture = "pic-lunar";
	$tmpl->installCSS( $webRoot . "/css/con-tact.css");
	$tmpl->printTop();
?>

<div id="main">
	<div id="basicPage" class="big">
		
		<h2>Contact Us</h2>
		
		<p id="contactMsg">
			Give us a <a href="mailto:jbellsey@gmail.com">shout</a>!
			What web site or project are you working on? What is your current situation,
			including constraints? This is just a conversation-starter; we’ll do most of the heavy
			lifting on the phone.
		</p>

	</div>
</div>

<?php 
	$tmpl->printBottom();