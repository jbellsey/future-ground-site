var fs = require('fs'),
    path = require('path'),
    runner = require("child_process");

function finder(dir, regex) {
  return fs.readdirSync(dir).filter(f => f.match(regex));
}

function deleteFolderRecursive(path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function(file){
      var curPath = path + "/" + file;
      if (fs.lstatSync(curPath).isDirectory())
        deleteFolderRecursive(curPath);
      else
        fs.unlinkSync(curPath);
    });
    fs.rmdirSync(path);
  }
}

function copyRecursiveSync(src, dest) {
  var exists = fs.existsSync(src);
  var stats = exists && fs.statSync(src);
  var isDirectory = exists && stats.isDirectory();
  if (exists && isDirectory) {
    fs.mkdirSync(dest);
    fs.readdirSync(src).forEach(function(childItemName) {
      copyRecursiveSync(path.join(src, childItemName),
        path.join(dest, childItemName));
    });
  } else {
    fs.linkSync(src, dest);
  }
}

const outputDir = './public_html';

deleteFolderRecursive(outputDir);
fs.mkdirSync(outputDir);
fs.mkdirSync(outputDir + '/portfolio');
fs.mkdirSync(outputDir + '/pfolio');

['css', 'i', 'js'].forEach(dir => {
  copyRecursiveSync(`./${dir}`, outputDir + `/${dir}`);
});

copyRecursiveSync(`./pfolio/icons`, outputDir + `/pfolio/icons`);
copyRecursiveSync(`./pfolio/struts`, outputDir + `/pfolio/struts`);


// copy the redirects file
fs.linkSync('./_redirects', `${outputDir}/_redirects`);


//-----------------
// copy statics


//-----------------
// process top-level files
var re = new RegExp('\.php$'),
    files = finder('.', re);

files.forEach(f => {
  runner.exec(`php ${f} > ${outputDir}/${f.replace(/\.php$/, '.html')}`);
});


//-----------------
// and make the portfolio files
files = finder('./pfolio/data', re);

files.forEach(f => {
  var pName = f.replace(/\.php$/, '');
  runner.exec(`php portfolio-server.php ${pName} > ${outputDir}/portfolio/${pName}.html`);
  //runner.exec(`php portfolio-server.php?id=${pName} > ${outputDir}/portfolio/${pName}.html`);
});

