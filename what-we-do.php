<?php 
	require_once("lib/template.php");
	
	$tmpl->title = "What We Do";
	$tmpl->metaDesc = "";
	$tmpl->activeNav = "whatWeDo";
	$tmpl->bgPicture = "pic-sii";
	$tmpl->installCSS( $tmpl->webRoot() . "/css/what-we-do.css");

	$tmpl->printTop();
?>

<div id="main">
	<div id="copy">
		<h2>We build apps. And stuff.</h2>
		
		<p class="big">Business apps and web sites are our bread and butter, and we do technology projects of all
			shapes and sizes. Tech is just a foot in the door though, as our expertise is in the bridging of business strategy and technology.</p>
	</div>


	<div id="we-do">
		<div id="we-do-list">

			<a class="desc" id="desc1" href="javascript:;" data-track="Apps">
				<h3>Business apps</h3>
				<p>Inefficient business processes getting you down? We can
					rebuild them. We have the technology.</p>
			</a>
			<a class="desc" id="desc2" href="javascript:;" data-track="Web sites">
				<h3>Web sites</h3>
				<p>Simple or custom? Basic brochure or a shopping cart
					and an art gallery? Anything goes.</p>
			</a>
			<a class="desc" id="desc3" href="javascript:;" data-track="Tech support">
				<h3>Technology support</h3>
				<p>Hosting issues, database setup,
					online videos... The stuff you’re not expected to know. </p>
			</a>
			<a class="desc" id="desc4" href="javascript:;" data-track="Strategic consulting">
				<h3>Strategic consulting</h3>
				<p>For when you have a tech team in place, and just need
					insight from someone who’s learned the ropes.</p>
			</a>
			<a class="desc" id="desc5" href="javascript:;" data-track="E-Books etc.">
				<h3>E-books, apps, audio &amp; video galleries...and stuff.</h3>
				
			</a>
		
		</div><?php
		
		?><div id="we-do-defns">
			<div class="defn" id="defn1">
				<a href="javascript:;"><img
						src="<?php echo $tmpl->webRoot() ?>/i/what-we-do/launch.jpg" width="470" height="76"
						alt="Product Launches"></a>
				<h4>Business apps<span><a href="javascript:;" class="inv">Close</a></span></h4>

				<div class="defCon">
					<p>Are you running your business with Excel? Trying to turn QuickBooks
						or AWeber or Gmail into a jack of all trades?</p>

					<p>Well, you’re not alone. It’s the way most business start out. But to
						grow, you need to use the right tools for the job. And often, there
						isn’t a tool out there perfectly suited to your business.</p>

					<p>Let us craft a custom app for you. It can be built for mobile devices
						for your in-the-field reps, or designed for your admins to do
						their work in the office. </p>

					<p>Custom apps are for everyone these days. And they’re not as
						expensive as you may think.</p>
				</div>
			</div>

			<div class="defn" id="defn2">
				<a href="javascript:;"><img
					src="<?php echo $tmpl->webRoot() ?>/i/what-we-do/web-sites.jpg" width="470" height="76"
					alt="Web Sites"></a>
				<h4>Web sites<span><a href="javascript:;" class="inv">Close</a></span></h4>
                <div class="defCon">
                    <p>Your web site is your brand. Your mission. Your calling card to the world.
                        And it’s often your first impression.</p>

                    <p>Your site might need to be simple and beautiful, or rich with information and
                        full of detail. Whatever your end goal, a guide with experience in the
                        creative process of webcraft will help you take it to a higher level.</p>

                    <p>We use Wordpress for most web sites. Lucky for you, we know the ins and outs,
                        the deeper possibilities it offers. We can build you a Wordpress site that
                        looks as unique as you do.</p>

                    <p>Of course, we speak all the buzzwords...just not in public. If we’ve done our
                        job right, the technology will be transparent. And like a piece of art, you’ll
                        be left only with the emotional resonance that comes when vision and execution unite.</p>

                    <p>What kind of web sites do we build? We’ve built all kinds, from single-page
                        splashes to multi-thousand-page sites with databases, blogs, and interactive
                        widgets. There are no boundaries here.</p>
                </div>
			</div>
		
			<div class="defn" id="defn3">
				<a href="javascript:;"><img
					src="<?php echo $tmpl->webRoot() ?>/i/what-we-do/tech-support.jpg" width="470" height="76"
					alt="Technical Support"></a>
				<h4>Technology support<span><a href="javascript:;" class="inv">Close</a></span></h4>
                <div class="defCon">
                    <p>Whatever type of business you own, you may not be fluent in all of the skills that
                        are needed to run your business. Accounting. Public relations. And back-end technology.</p>

                    <p>Whether you have to move your site to a new server to gain more capacity, set up your
                        email marketing system to take online registrations, or need to spam-proof the contact
                        form on your web site, let us handle the heavy tech stuff. And you can do what you do
                        best: run your business.

                    <p class="dotTop">Shopping cart setup • Databases • Hosting • Software installation
                        and configuration&nbsp;• Streaming
                        videos&nbsp;• Teleseminars&nbsp;• Site analytics &amp; visitor
                        profiling&nbsp;• 
                        Newsletters &amp; email list management&nbsp;• Affiliate
                        software&nbsp;setup&nbsp;• And&nbsp;more...
                </div>
			</div>
		
			<div class="defn" id="defn4">
				<a href="javascript:;"><img
					src="<?php echo $tmpl->webRoot() ?>/i/what-we-do/strategy.jpg" width="470" height="76" alt="Strategic Consulting"></a>
				<h4>Strategic consulting<span><a href="javascript:;" class="inv">Close</a></span></h4>
                <div class="defCon">
                    <p>Here at Future Ground, we don't need to be the ones pushing pixels around, or tinkering
                        with Wordpress plugins. If you already have people you know and trust, by all means use them!</p>

                    <p>Bring us in to fill the gaps. We’ve already made most of the big mistakes (and learned from
                        them!), so we can help keep your team from making them too. Having an experienced support
                        structure pays for itself many times over.</p>

                    <p>We can help you plan your next teleseminar, or design new user experiences, or select
                        technologies for your new platform. It’s not easy to narrowly define the value of 15
                        years of deep web experience. Let’s just say: it’s a lot.
                </div>
			</div>

			<div class="defn" id="defn5">
				<a href="javascript:;"><img
					src="<?php echo $tmpl->webRoot() ?>/i/what-we-do/ebooks.jpg" width="470" height="76" alt="E-Books, Apps, Etc"></a>
				<h4>E-books, apps &amp; stuff<span><a href="javascript:;" class="inv">Close</a></span></h4>
                <div class="defCon">
                    <p>While web sites and technology consulting are our main offering, we work on plenty of projects
                        that don’t fit neatly into that category.</p>

                    <p>We have production people who build e-books with elegance and simplicity. Programmers who do
                        mobile apps, designers who build user experiences for tablets. We have music specialists who
                        build audio galleries, video post-production artists, e-commerce wizards, site analytics gurus,
                        and more.</p>

                    <p>If your project has anything at all to do with technology, we probably have access to the
                        right genius for the job.</p>
                </div>
			</div>
		</div>
		
	</div>

</div>

<?php 
	$tmpl->printBottom();