<?php 
	$pfolio->metaDesc = "";
	$pfolio->metaKeys = "";
	$pfolio->title = "Bill McCart Consulting";
	$pfolio->body = <<<HTML
	
<div id="portfolioCopy">

	<p>After many years of providing operations leadership for Fortune 500 firms and educational organizations, Bill McCart decided to start his own business. With experience running point for a multi-million dollar e-learning corporation, he is now offering services to teachers looking to take their message to a wider audience through teleseminars and online courses.</p>
	
	<h3>The project</h3>
	
	<p>We experimented with crowdsourcing the design and hit the jackpot: this is a very elegant brand representation.</p>
	
	<p>Design in hand, a fully customized Wordpress theme was built from the ground up. It has customizable widgets, shortcodes, sliders, admin pages, a blog, and all the goodies you find in a pre-built Wordpress theme, but without the bloated and badly-written code. Clean and elegant design, implemented with clean and elegant programming, makes for a strong platform for the future.

	<p><a href="http://www.billmccart.com/" class="inv outbound">Visit web site</a></p>
	

</div>

<div id="portfolioStrut">

	<a href="http://www.billmccart.com/" class="outbound"><img src="$webRoot/pfolio/struts/mccart.png" width="363"></a>

</div>

HTML;
