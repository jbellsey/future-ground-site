<?php 
	$pfolio->metaDesc = "";
	$pfolio->metaKeys = "";
	$pfolio->title = "EnlightenNext";
	$pfolio->body = <<<HTML

<div id="portfolioCopy">
	<p>EnlightenNext began its life as a small publication called <em>What Is Enlightenment?</em> magazine. Somewhere in its two-decade lifetime, during the transition to one of the best-looking magazines of any genre, it became a sophisticated web presence serving hundreds of thousands of visitors a year.</p>
	
	<p>The main web site for EnlightenNext consists of about four thousand web pages from the dozens of issues of the magazine. All content-managed, cross-referenced by subject and contributing author. It serves as a dream tool for anyone interested in researching the cutting edge of consciousness and culture.</p>

	<h3>The project</h3>
	
	<p>The EnlightenNext web site was built over a decade, and had many twists, turns, and complete overhauls. Here are a few of the highlights:</p>
	
	<ul>
		<li>Weekly audio-visual service with some sexy buzzwords: CMS, CDN, SQL Server database, live streaming, newsletter delivery
		
		<li>Customized online payment gateway for recurring billing, with sophisticated handling of failed credit cards
		
		<li>Tie-ins to printed content, including subscriber self-management
		
		<li>Theme pages, organizing multi-media content into subject areas for target audiences (and markets)
		
		<li>Many other custom back-end systems with fancy names, including: an A/B testing engine, data integration across a handful of offsite platforms and technology partners, a hand-rolled email newsletter delivery service, a cross-referenced database of authors and topics, and more
	</ul>

	<p><strong>Note</strong>: The site is no longer operational. The magazine ceased publication in 2011,
	and the organization has since folded.
	
</div>

<div id="portfolioStrut">

	<img src="$webRoot/pfolio/struts/enx.png" width="375">

</div>

HTML;
