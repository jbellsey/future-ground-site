<?php
$pfolio->metaDesc = "";
$pfolio->metaKeys = "";
$pfolio->title = "Vital Medicine";
$pfolio->body = <<<HTML

<div id="portfolioCopy">

	<p>Deb Zucker called and said she was ready for Vital Medicine 2.0. Not just a redesign of her website,
	but a redesign of her entire business. </p>

	<h3>The project</h3>

	<p>Now that’s juicy! We worked together with Dr. Deb on every facet of the business. Yes,
	we did a full redesign of her brand and her web site. But there was a lot more too:
	<p>
	<ul>
	    <li>We conceived and scripted an introductory video for instant rapport on the web site. Shot and edited by
	    the masterful <a href="http://www.jedshare.com/" class="inv outbound">Jed Share</a>,
	    we gave Deb a great way to invite people into the deep work that she’s doing.

	    <li>We did a full re-architecting of her web presence. The site structure on her previous web site was (ahem)
	     messy, desperately in need of simplification and organization. BOOM, done.

	   <li>We customized her Wordpress installation to allow for an elegant approach to event and program management.

	    <li>And, of course, we did a full launch of her new web site, including audio pages,
	    a sophisticated engine for testimonials, and a gorgeous design that looks great on phones as well.
	</ul>

	<p><a href="http://vitalmedicine.com/" class="inv outbound">Visit web site</a></p>

</div>

<div id="portfolioStrut">

	<a href="http://vitalmedicine.com/" class="outbound"><img src="$webRoot/pfolio/struts/vital-med.png"
	width="370"></a>

</div>

HTML;
