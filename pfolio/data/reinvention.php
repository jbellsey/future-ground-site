<?php 
	$pfolio->metaDesc = "";
	$pfolio->metaKeys = "";
	$pfolio->title = "Reinvention Summit";
	$pfolio->body = <<<HTML
	
<div id="portfolioCopy">

	<p>Storytelling is most definitely in. When the people at GetStoried needed some experienced help with putting together their product launch, they came to us.</p>
	
	<h3>The project</h3>
	
	<p>The Reinvention Summit is the largest-ever virtual gathering of marketers, change-makers, and creators who use storytelling as the secret to reinvention. Hundreds of participants, twenty speakers, over a five-day period...that’s a lot of moving parts.</p>
	
	<p>The tech team had already been lined up, but hadn’t ever worked on a project this complex. Future Ground was called in to consult, and to ensure that the event — from the sales process to the delivery — went without a hitch.
	
	<p>Building a squeeze page for signups, a sales page for registration...that goes without saying. Plus a membership site for private content, with ties to LinkedIn, Twitter, and Facebook. Add in the unavoidable back-end quirks and hassles, and you have a large pile of mess waiting to happen. When it comes to an event like this, creating a seamless experience for your users is a must, which is why the Summit brought in a seasoned team to create a robust technology platform.</p>
	
	<p><a href="http://www.reinventionsummit.com/" class="inv outbound">Visit web site</a></p>

</div>

<div id="portfolioStrut">

	<a href="http://www.reinventionsummit.com/" class="outbound"><img src="$webRoot/pfolio/struts/reinvention.png" width="370"></a>

</div>


HTML;
