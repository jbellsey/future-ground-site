<?php 
	$pfolio->metaDesc = "";
	$pfolio->metaKeys = "";
	$pfolio->title = "Enlightened Embodiment";
	$pfolio->body = <<<HTML
	
<div id="portfolioCopy">

	<p>Sexologist Elizabeth Ferguson decided it was time to make the leap. While her Bay Area consulting gigs were paying the bills, her passion (and her future) lay in the field of transformative sexual development.</p>
	
	<h3>The project</h3>
	
	<p>Elizabeth needed rapid deployment and an inexpensive platform upon which she could build her business and her reputation. After selecting a prebuilt Wordpress theme, we set about doing some light design work, including logotype, banner images, and a color scheme she loved. A little back-end wizardry (hosting, DNS, and email setup) and she was on her way. 

	<p>While Elizabeth’s web site isn’t as visually complex as EnlightenNext or Bill McCart’s site, it’s a great example of what can be done on a budget when you have the right team in place.
	
	<p><a href="http://enlightenedembodiment.com/" class="inv outbound">Visit web site</a></p>

</div>

<div id="portfolioStrut">

	<a href="http://enlightenedembodiment.com/" class="outbound"><img src="$webRoot/pfolio/struts/embodiment.png" width="369"></a>

</div>
HTML;
