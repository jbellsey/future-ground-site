<?php 
	$pfolio->metaDesc = "";
	$pfolio->metaKeys = "";
	$pfolio->title = "Jean Houston’s Online Course: <br>“Awakening To Your Life’s Purpose”";
	$pfolio->body = <<<HTML
	
<div id="portfolioCopy">

	<p>Human potential pioneer Jean Houston needed an online offering. With a lifetime of accomplishment and an incomparable impact on cultural leaders from Hillary Clinton to Margaret Mead, her legacy was in need of a course that could be accessed by anyone from anywhere around the world.</p>
	
	<h3>The project</h3>
	
	<p>The course took shape over several months of planning and development. Signups are still open...check it out!</p>
	
	<ul>
		<li>Squeeze page for email signups
		<li>Affiliate program for maximizing reach
		<li>Sales page linked to shopping cart offerings (including installment-plan options)
		<li>Live teleconferences for pre-sales buzz
		<li>Members-only site with community forums and Q&A
		<li>Time-delayed content delivery, to make rolling signups feel more like a live course
	</ul>
	
	<p><a href="http://purposeanddestiny.net/" class="inv outbound">Visit web site</a></p>

</div>

<div id="portfolioStrut">

	<a href="http://purposeanddestiny.net/" class="outbound"><img src="$webRoot/pfolio/struts/houston.png" width="360"></a>

</div>

HTML;
