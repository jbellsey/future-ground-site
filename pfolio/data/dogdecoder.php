<?php
$pfolio->metaDesc = "";
$pfolio->metaKeys = "";
$pfolio->title = "Dog Decoder iPhone App";
$pfolio->body = <<<HTML

<div id="portfolioCopy">

	<p>A genius dog trainer called us up and said she was sick of seeing people get bitten. She played us a video
	of a newsman who was a victim of a dog who had never hurt anyone before. “I saw that bite coming ten minutes
	before it happened. Why didn’t he??”</p>

	<h3>The project</h3>

	<p>We worked with <a href="http://shewhisperer.com" class="inv outbound">Jill Breitner</a> to create a fun,
	educational app for dog lovers. We brought in the best dog illustrator around, and she nailed it: <a href="http://doggiedrawings.net/" class="inv outbound">Lili Chin</a> turned Jill’s ideas into a
	set of illustrations that captured the detail we needed, without losing her signature sense of whimsy and delight.

	<p>The biggest (and most fun) part of the job was helping Jill articulate her vision. Taking her from the passion
	 to prevent dog bites, through the long and wonderful process of research, storyboarding, design, programming,
	 and finally deployment — we truly turned her vision into reality.
	<p>

	<p><a href="http://dogdecoder.com/" class="inv outbound">Visit web site</a></p>

</div>

<div id="portfolioStrut">

	<a href="http://dogdecoder.com/" class="outbound"><img src="$webRoot/pfolio/struts/dogdecoder.png" width="375"></a>

</div>
HTML;
