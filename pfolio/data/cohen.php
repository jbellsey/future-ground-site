<?php 
	$pfolio->metaDesc = "";
	$pfolio->metaKeys = "";
	$pfolio->title = "Andrew Cohen";
	$pfolio->body = <<<HTML
	
<div id="portfolioCopy">

	<p>Spiritual teacher. Author. Retreat leader. Creative visionary. Rude boy. And now, deposed guru.</p>
	
	<p>(At long last, Andrew has been removed from leadership roles in his now-defunct organization. Despite this,
	the work we did for his web site was pretty cool.)
	
	<h3>The project</h3>
	
	<p>At the heart of the web site are Andrew’s spiritual teachings. Many are offered for free on the site, some are available for purchase or download, and there are live events as well:</p>
	
	<ul>
		<li>Content-managed articles, audios, and videos
		<li>Shopping cart for purchasing physical product (books, CDs, etc.) and digital product (MP3s)
		<li>Custom retreat-registration engine for international events and multiple currencies
		<li>Interactive tools for exploring complex spiritual teachings
	</ul>
	
	<p><a href="http://www.andrewcohen.org/" class="inv outbound">Visit web site</a></p>

</div>

<div id="portfolioStrut">

	<a href="http://www.andrewcohen.org/" class="outbound"><img src="$webRoot/pfolio/struts/ac.png" width="373"></a>

</div>
HTML;
