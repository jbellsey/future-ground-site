
$(function() {
    "use strict";

    var GA = {

        event: function(categ, action) {
            try {
                _gat._getTrackerByName()._trackEvent(categ, action);
            }
            catch(e) {}

        },

        outboundLink: function(url) {
            GA.event('Outbound Links', action)
        }
    };

    //------- ALL PAGES
    //
    // track outbound links
    $('a.outbound').each(function() {
        $(this)
            .attr('target', '_blank')
            .click(function() {
                GA.outboundLink(this.href);
            });
    });

    //------- CONTACT PAGE
    //
    /*
    $('#contactForm').submit(function() {
        var f = $(this);

        $.post("submit.php", f.serialize(), function(data) {

            if (+data === 1) {	// success
                // gray out form
                f.css('opacity', '0.2')
                 .find('input,textarea')
                    .prop("disabled", true);

                // change message & highlight it
                $('#contactMsg')
                    .html("Thanks for writing! We're currently orbiting Titan, so response time should be about a day.")
                    .css("background-color", "#6b8b75");

                GA.event("Contact Form", $('input#email').val());
            }
            else {
                alert("A bit more study of US history will help you get through our spam filter!");
            }
        });

        return false;
    });
    */

    //------- PORTFOLIO PAGES
    //
    var car = $('#portfolioCarousel');
    if (car.length > 0) {   // not strictly needed

        // prep the B&W background images
        car.find('a span').each(function () {
            var span = $(this),
                img = span.find('img'),
                src = img.attr('src').replace('.png', '-bw.png');
            span.attr('style', 'background-image:url(' + src + ')');
        });

        car.jcarousel({start: startCarouselAt});
    }

    //------- WHAT WE DO
    //

    var openBox = 0;

    // open up a different "tab"
    function swap(i) {
        var trackThis, obj, j;

        if (i === openBox) {
            $('#defn'+openBox).toggleClass('fullBox');
            $('.defn').removeClass('hideBox');
            $('#desc'+openBox).removeClass('selBox');
            openBox = 0;
        }
        else {
            $('#defn'+openBox).toggleClass('fullBox');
            $('#desc'+openBox).removeClass('selBox');
            $('#defn'+i).toggleClass('fullBox');
            trackThis = $('#desc'+i).addClass('selBox').data('track');
            openBox = i;

            for (j = 1; j <= 10; ++j) {
                obj = $('#defn'+j);
                if (obj.length === 0)
                    break;

                if (i !== j)
                    obj.addClass('hideBox');
                else
                    obj.removeClass('hideBox');
            }

            GA.event("What We Do", trackThis);
        }
    }

    // the 4th character of the object's ID holds the numeric key
    function c4(obj) {
        return parseInt($(obj).attr('id').charAt(4), 10);
    }

    // interactions
    $('.desc')
        .click(function() {
            swap(c4(this));
        })
        .hover(function() {
            $('#defn'+c4(this)).toggleClass('hover');
        });

    $('.defn').hover(function() {
        $('#desc'+c4(this)).toggleClass('hover');
    });

    // the main action buttons on the "what we do" page
    $('.defn>a:first-child, .defn a.inv').click(function() {
        swap(c4($(this).closest('.defn')));
    });
});

