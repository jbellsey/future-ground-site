/*
 * JSB: config values for asamuzak's script
 *
 *      docs here:  http://asamuzak.jp/test/textshadow_test_ie_en
 */


// for larger text: we use a darker shadow with a smaller offset
// for smaller text: we use a lighter shadow with a larger offset


var ieLargeTextSelectors = "";
		//'h2, p, button.orange, #home p';

var ieSmallTextSelectors = "";
		// 'h3, #roadmap p';

//--------------------

var ieTextShadowValues = [

	// larger text
	{ sel: ieLargeTextSelectors, shadow: '0 0 0 rgb(33,33,33)' },
	
	// smaller text
	{ sel: ieSmallTextSelectors, shadow: '1px 1px 0 rgb(66,66,66)' }

];
