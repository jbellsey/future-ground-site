<?php 
	require_once("lib/template.php");
	$webRoot = $tmpl->webRoot();
	
	$myStyles = <<<STYLE
        <style>
            a.outbound img { margin-top: -18px }
            @media only screen and (max-width:750px) {
                img.header { width: 225px; height:225px }
            }
            @media only screen and (max-width:450px) {
                img.header { width: 125px; height:125px }
            }
        </style>
STYLE;

	$tmpl->title = "About Us";
	$tmpl->metaDesc = "Web development, web site creation in Marin County, California";
	$tmpl->activeNav = "whoWeAre";
	$tmpl->bgPicture = "pic-naut";
	$tmpl->installHeader( $myStyles);
	$tmpl->printTop();
?>

<div id="main">
	<div id="basicPage" class="big">
		
		<h2>Who We Are</h2>
		
			<p><img class="dropshad floatR header" src="<?php echo $tmpl->webRoot() ?>/i/headshot.jpg" width="316"
                    height="298">
			Future Ground is more of a “me” than a “we,” but that doesn’t come close to telling the whole story.</p>
			
			<p>My name is Jeff Bellsey, and I hold the core positions at Future Ground. As the main strategist and technologist, there are many jobs for which I am both conducting the orchestra and playing all of the instruments.</p>
			
			<p>However, my real role (and passion) is coordinating solutions of complex puzzles, bridging the gap between vision and implementation. So in many cases, my job is less about bits and bytes, and more about communication and synergy, finding depth and clarity in a technological and strategic thicket.</p>
			
			<p>Fear not: for bigger jobs, there are others in the wings. Designers, programmers, marketers, copywriters, project managers. A small infantry is available when needed.
			
			<p>You can read more about me on my LinkedIn profile: <a href="http://www.linkedin.com/in/jeffbellsey" class="outbound"><img src="<?php echo $tmpl->webRoot() ?>/i/in.png"></a></p>
		
		<h3 class="dots">Where we are</h3>
		
			<p>Future Ground is based in Marin County, California. So if you’re in the Bay, we can do our work in person. Otherwise, Skype makes it easy to work together no matter where you live.
		
		<h3 class="dots">About “Future Ground”</h3>
		
			<p>Space-age kidding aside: The meaning of our name is simple. The technology we build today will be the ground of the future. And our ethic is to build that ground strong and solid.</p>
			
			<?php if (0) { ?>
			<p>Why? Because by building it right – as right as you reasonably can – the first time,  you pre-empt the inevitable feature-request conversations. 
			
			ensure that the questions about adding new features are answered with 
			
			you don’t have to look a stakeholder in the eye and tell them 
			<?php } ?>
		
	</div>
</div>

<?php 
	$tmpl->printBottom();