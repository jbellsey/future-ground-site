<?php 
	require_once("lib/template.php");
	$webRoot = $tmpl->webRoot();

	// key is the URL (lowercase), also the include file with the body
	//
	$entries = array(
		"dogdecoder"	=> array( "img" => "dog",		"text" => "Dog Decoder"),
		"vital-medicine"=> array( "img" => "vital-med",	"text" => "Vital Medicine"),
		"enlightennext"	=> array( "img" => "enx",		"text" => "EnlightenNext"),
		"reinvention"	=> array( "img" => "reinvention","text" => "Reinvention Summit"),
		//"cito"			=> array( "img" => "cito",		"text" => "Calling In The One"),
		"houston"		=> array( "img" => "houston",	"text" => "Your Life’s Purpose"),
		"embodiment"	=> array( "img" => "embodiment", "text" => "Embodiment"),
		"cohen"			=> array( "img" => "cohen",		"text" => "Andrew Cohen"),
		"mccart"		=> array( "img" => "mccart",	"text" => "McCart Consulting")
	);

	$id = "";
	if ( isset( $_GET['id']))
		$id =  strtolower( trim( $_GET['id'] ));
	else $id = $argv[1];	// given to us by the build script

	if ( strlen($id) == 0 || !array_key_exists( $id, $entries)) {
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: $webRoot/what/we've/done");
		exit();
	}

	$thisEntry = $entries[$id];
	$thisEntry['url'] = $id;

	//-----------

	class PortfolioData {
		public $metaDesc;
		public $metaKeys;
		public $title;		// used in H2 and the <title> tag
		public $body;		// use a long string (HEREDOC)
	};
	$pfolio = new PortfolioData;

	// load the details for this portfolio entry
	require_once( "pfolio/data/$id.php");

	//-----------

	$tmpl->title = "Portfolio: " . $pfolio->title;
	$tmpl->metaDesc = $pfolio->metaDesc;
	$tmpl->activeNav = "whatWeveDone";
	$tmpl->bgPicture = "pic-shuttle";
	$tmpl->installJS( $tmpl->webRoot() . "/js/jcarousel.min.js");
	$tmpl->installCSS( $tmpl->webRoot() . "/css/carousel.css");
	$tmpl->installCSS( $tmpl->webRoot() . "/css/portfolio.css");
	$tmpl->printTop();
?>


<div id="main">
	<div id="basicPage">

		<ul id="portfolioCarousel" class="jcarousel-skin-future">
		<?php 
			$i = 1; $startAt = $i;
			foreach ($entries as $url => $data) {
				if ( $url == $id) $startAt = $i;
				echo "<li><a href=\"$webRoot/portfolio/$url\"><span>" .
					"<img src=\"$webRoot/pfolio/icons/" . $data['img'] . ".png\" /></span>" . $data['text']. "</a></li>";
				++$i;
			}
		?>
		</ul>
		
		<h2><?php echo $pfolio->title; ?></h2>
		<?php echo $pfolio->body;
			// <a href="$webRoot/what/we've/done" class="inv">Main portfolio page</a> ?>
		
	</div>
</div>

<script>
	startCarouselAt = <?php echo 1; // $startAt; // or ONE (do not eliminate this line) ?>;
</script>

<?php 
	$tmpl->printBottom();