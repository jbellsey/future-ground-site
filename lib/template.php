<?php
	// global instance is created at the end of this file
	//
	//		$tmpl = new FGTemplate;
	//
	// basic usage:
	//
	//		$tmpl->title = "This page title";
	//		$tmpl->metaDesc = "yup yup";
	//		$tmpl->fbDesc = "";
	//		$tmpl->bgPicture = "...";
	//		$tmpl->installCSS( "../css/stuff.css");
	//		$tmpl->printTop();
	//			...
	//		$tmpl->printBottom();
	//

	
	class FGTemplate {
		public $title = "";
		public $metaDesc = "";
		public $fbDesc = "Web sites, technology consulting, and strategic advice for the space age.";
        // TODO: these should be moved to enums
		public $activeNav = "";				// home | whatWeDo | whatWeveDone | testim | whoWeAre
		public $layout = "basic";			// home | basic | gallery | wide  (added as body ID)
		public $bgPicture = "";				// added as a class to <body>... see class list in styles
		public $loadScripts = array();
		public $loadCSS = array();
		public $customHeaders = array();	// custom header directives (not CSS or JS)

        private $_root = "";

        function __construct() {
            $svr = strtolower( $_SERVER['SERVER_NAME']);
            $isLiveSite = (strpos($svr,'local') === false);

            $devRoot = "/future";
            $liveRoot = "";
            $this->_root = $isLiveSite ? $liveRoot : $devRoot;
        }
		
		// header management. add CSS or JS files in the two functions below
		//
		// NOTE: use the public function webRoot() to ensure the file 
		//	location works! e.g., $tmpl->installCSS( $tmpl->webRoot() . "/css/x.css");
		//

		// pass in a custom CSS file name or array 
		// NOTE: these are all set up for SCREEN media;
		//	if you need PRINT style sheets, use installHeader()
		public function installCSS( $f) { $this->appendToArray( $f, $this->loadCSS); }
		
		// pass in a custom JS file name or array
		public function installJS( $f) { $this->appendToArray( $f, $this->loadScripts); }
		
		// pass in a custom header (string or array). these are fully-
		// specified declarations to include in the <head> section of the file
		//
		public function installHeader( $f) { $this->appendToArray( $f, $this->customHeaders); }
		
			// $f: string or array of strings to append to...
			// $a: the master array
			private function appendToArray( $f, &$a) {
				if ( is_array( $f)) {
					foreach( $f as $fn)
						$a[] = $fn;
				}
				else
					$a[] = $f;
			}
		
		//-----------------------------------
		
		// prints the WEB_ROOT directory
		//
		private function root() {
			echo $this->_root;
		}
		
		// this is the PUBLIC version; it
		//	RETURNS the root instead of PRINTING it.
		//	it's just a pass-thru to the $config var...
		//	note that the return value DOES NOT have a trailing slash
		//
		public function webRoot() {
            return $this->_root;
		}

        public function activeNavClass($val)   {
            if ( $this->activeNav == $val)
                echo ' class="active"';
        }


        // here we go... printing out the template
		//
		public function printTop() {
			
			// filter dupes
			$this->loadScripts	= array_unique( $this->loadScripts);
			$this->loadCSS		= array_unique( $this->loadCSS);
			$this->customHeaders = array_unique( $this->customHeaders);
			
			// other adjustments
			if ( !$this->title)
				$this->title = "Future Ground";
			elseif ( $this->layout != "home")
				$this->title .= " | Future Ground";
			
			// clean up the title tag
			$this->title = preg_replace( array( '/<br>/', '/<b>/', '/<\/b>/' ), 
				'', $this->title);
?>
<!DOCTYPE html>
<!--[if lte IE 8 ]> <html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]> <html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="" lang="en"> <!--<![endif]-->
<head>
	<title><?= $this->title ?></title>
	<meta charset="utf-8">
	<?php if ( $this->metaDesc) { ?><meta name="description" content="<?= $this->metaDesc; ?>" /><?php } ?>
	<link rel="Shortcut Icon" type="image/ico" href="<?= $this->webRoot() ?>/i/favicon.ico" />
	<link rel="stylesheet" href="<?= $this->webRoot() ?>/css/reset.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?= $this->webRoot() ?>/css/fonts/salaman.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?= $this->webRoot() ?>/css/future.css" type="text/css" media="screen" />


<!--[if lte IE 9]>
	<link rel="stylesheet" href="<?= $this->webRoot() ?>/css/ie.css" type="text/css" media="screen" />
<![endif]-->

	<?php
		// user-requested headers (CSS, JS, and custom) (see comments above)
		//
//		foreach( $this->loadScripts as $f)
//			echo "<script src='$f'></script>";
			
		foreach( $this->loadCSS as $f)
		echo "<link rel=stylesheet type='text/css' href='$f'>";

		foreach( $this->customHeaders as $f)
			echo $f;
	?>
	<?php /* for the future:
		<script src="<?= $this->webRoot() ?>/js/ieTextShadowEngine.js"></script>
		<script src="<?= $this->webRoot() ?>/js/ieTextShadowConfig.js"></script>
		<!-- fav and touch icons -->
		<!-- <link rel="shortcut icon" href="images/favicon.ico">
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->
		*/
	?>
	<meta property="fb:app_id" content="217561168350128" /> 
	<meta property="fb:admins" content="100000558131209" /> 
	<meta property="og:title" content="<?= $this->title ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="<?= $this->fbDesc ?>" />
	<meta property="og:url" content="<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ?>">
	<meta property="og:image" content="http://<?= $_SERVER['SERVER_NAME']; echo $this->webRoot() ?>/i/fb200.png" />
    <meta name=viewport content="width=device-width, initial-scale=1">

	<script>
	// google analytics 
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-8850631-6']);
	_gaq.push(['_trackPageview']);
	
	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>

</head>
<body id="<?= $this->layout ?>" class="<?= $this->bgPicture ?>">

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=217561168350128";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<div id="topNav">
	<ul>
		<li<?php $this->activeNavClass('whatWeDo') ?>>
			<a href="<?= $this->webRoot() ?>/what/we/do" id="whatWeDo"><img src="<?= $this->webRoot() ?>/i/nav/tools.png"><b>What we
                    do</b></a></li>
		<li<?php $this->activeNavClass('whatWeveDone') ?>>
			<a href="<?= $this->webRoot() ?>/what/we've/done" id="whatWeDid"><img src="<?= $this->webRoot() ?>/i/nav/launch.png"><b>What
                    we&rsquo;ve done</b></a></li>
		<li<?php $this->activeNavClass('testim') ?>>
				<a href="<?= $this->webRoot() ?>/what/they/say" id="whatTheySay"><img src="<?= $this->webRoot() ?>/i/nav/quote.png"><b>What
                        they say</b></a></li>
		<li<?php $this->activeNavClass('whoWeAre') ?>>
				<a href="<?= $this->webRoot() ?>/who/we/are" id="whoWeAre"><img src="<?= $this->webRoot() ?>/i/nav/who.png"><b>Who we
                        are</b><span></span></a></li>
	</ul>
</div>

<?php if ( $this->layout != "home") { ?>
<div id="headSection">
	<div id="headContainer">
		<h1><a href="<?= $this->webRoot() ?>/">Future Ground</a></h1>
		<?php if ($this->activeNav != "contact") { ?>
			<a class="orange" href="<?= $this->webRoot() ?>/contact">Contact us</a>
		<?php } ?>
		<div id="planets">
			<div id="p1"> </div>
			<div id="p2"> </div>
			<div id="p3"> </div>
		</div>
	</div>
</div>
<?php } ?>

<?php
		}	// end "printTop"

        //=============================

		public function printBottom() {
?>

<div id="footer">
	<div class="footContents">
		<div class="fb-like" data-href="http://www.futureground.net/" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>

		<span class="copyR">&copy; <?php echo date("Y"); ?> Jeff Bellsey. All rights reserved.</span>
	</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="<?= $this->webRoot() ?>/js/app.min.js"></script>
<?php
    foreach( $this->loadScripts as $f)
        echo "<script src='$f'></script>";
?>

</body>
</html>

<?php
		}	// end "printBottom"
	}	// end class

$tmpl = new FGTemplate;
?>
